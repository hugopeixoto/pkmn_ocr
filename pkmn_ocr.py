#!/usr/bin/env python3

# (1) System needs tesseract and opencv;
#     sudo dnf install tesseract opencv python3-opencv
#
# (2) Python needs:
#     pytesseract, pillow, Levenshtein, and numpy:
#     pip install --user pytesseract python-Levenshtein pillow numpy
#
# Examples:
# ./pkmn_ocr.py --input input/sv3/ --interactive --flavor --steps 4x gray otsu half
# ./pkmn_ocr.py --input input/sv3_dark --steps 4x gray otsu invert half

"""
usage: pkmn_ocr.py [-h] [--input INPUT_DIR] [--output OUTPUT_DIR] [--interactive] [--flavor] [--steps STEPS [STEPS ...]] [--oem OEM] [--psm PSM]

options:
  -h, --help            show this help message and exit
  --input INPUT_DIR     Path to load images from. Non-recursive.
  --output OUTPUT_DIR   Path to store intermediate images to. Default 'output'.
  --interactive         Interactive mode for correcting low-confidence guesses
  --flavor              Flavor text (instead of artist)
  --steps STEPS [STEPS ...]
                        Transformation steps to apply. Try upscale thresh or thresh upscale.
  --oem OEM             Passed to tesseract.
  --psm PSM             Passed onward to tesseract.
"""


import argparse
import json
import math
from pathlib import Path
import os
import tempfile
from typing import Callable, Tuple

import PIL.Image
from PIL import ImageOps
from PIL.Image import Image

import cv2
import pytesseract
import Levenshtein
import numpy

from prompt import prompt_user


# In case I forget how colors work
WHITE = (255, 255, 255, 255)
BLACK = (0, 0, 0, 255)


def distance(p1, p2) -> float:
    """
    Works for arbitrarily dimensioned points.
    """
    assert len(p1) == len(p2)
    return math.sqrt(
        sum((p2[i] - p1[i])**2 for i in range(len(p1)))
    )


def imgfilter_stream(imgdata, ffn: Callable[[Image], Image]):
    """
    Lazily apply a pixel filter to a stream of pixels.
    """
    for pix in imgdata:
        yield ffn(pix)


def imgfilter(image: Image, ffn: Callable[[Image], Image]) -> Image:
    """Apply a pixel filter to an image, giving a new image."""
    data = bytearray()
    for pixel in imgfilter_stream(image.getdata(), ffn):
        data.extend(bytes(pixel))
    tmp = PIL.Image.frombytes(image.mode, image.size, bytes(data))
    return tmp


def binarization_filter(
        threshold: float, negate: bool = False
) -> Callable[[int], int]:
    """
    Configurable threshold filter that only works for the Point filter.

    Generates a filter function and returns it.
    """
    int_thresh = min(max(round(threshold * 255), 0), 255)

    def _filter(x: int) -> int:
        return 255 * (bool(x <= int_thresh) == negate)
    return _filter


def proximity_filter(
        color=BLACK,
        bgcolor=WHITE,
        lower: int = 150,
        upper: int = 200
) -> Callable[[Tuple[int, ...]], Tuple[int, ...]]:
    """
    Works by selecting pixels that are within a proximity to a specified color.
    Between the lower and upper limits, a linear blending between
    the source pixel and the bgpixel color will occur.

    NB: The total distance from 0,0,0 to 255,255,255 is 441.
        0,0,0 to 127,127,127 is ~220.

    Generates a filter function and returns it.
    """

    _blend_range = upper - lower

    def _filter(pix):
        dist = distance(pix[:3], color[:3])

        if dist <= lower:
            return pix
        elif dist > upper:
            return bgcolor
        else:
            coeff = (dist - lower) / _blend_range
            coeff = min(max(coeff, 0), 1)
            inverse = 1.0 - coeff

            r = (pix[0] * coeff) + (bgcolor[0] * inverse)
            g = (pix[1] * coeff) + (bgcolor[1] * inverse)
            b = (pix[2] * coeff) + (bgcolor[2] * inverse)
            a = (pix[3] * coeff) + (bgcolor[3] * inverse)

            return (round(x) for x in (r, g, b, a))

    return _filter


def otsu_binarization(image):
    cvimg = numpy.array(image)
    x, cvimg_otsu = cv2.threshold(cvimg, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    return PIL.Image.fromarray(cvimg_otsu)


def gaussian_blur(image):
    cvimg = numpy.array(image)
    cvimg = cv2.GaussianBlur(cvimg, (5,5), 0)
    return PIL.Image.fromarray(cvimg)


def erode_dilate(image):
    cvimg = numpy.array(image)

    # I have no idea what I am doing here.
    kernel_erosion = numpy.ones((3,3), numpy.uint8)
    kernel_dilation = numpy.ones((3,3), numpy.uint8)

    cvimg = cv2.erode(cvimg, kernel_erosion, iterations=1)
    cvimg = cv2.dilate(cvimg, kernel_dilation, iterations=1)

    return PIL.Image.fromarray(cvimg)


def artist_crop(image):
    """
    Crop the Illustrator credit from a SV-era Pokemon card.
    """
    # (x) left, (y) upper, (x) right, (y) lower
    # SWSH: cropped = image.crop((45, 931, 235, 956))
    # works on everything except Energy, which has no artist.
    # bottom edge is a LITTLE aggressive, it cuts it real close.

    # SV: wide box that's too wide for trainers, but can fit a name like
    # "Kedamahadaitai Yawarakai".
    # cropped = image.crop((40, 937, 279, 957))

    # SV: box that's as wide as you can get without clipping into
    # elements on Trainers.  Some long names are clipped.
    cropped = image.crop((40, 937, 230, 957))
    return cropped


def flavor_crop(image):
    # (x) left, (y) upper, (x) right, (y) lower
    cropped = image.crop((225, 918, 225+469, 918+77))
    return cropped


def mkoutdir(outpath, *args):
    path = outpath.joinpath(*args)
    path.parent.mkdir(parents=True, exist_ok=True)
    return path


class TextPool:
    def __init__(self):
        self.entries = []
        self._cache = {}

    def load_artists(self, filename="pkmn_artist.json"):
        with open(filename, "r") as infile:
            artists = json.load(infile)

        for artist in artists:
            self.entries.append(f"Illus. {artist}")

    def load_zukan(self, filename='pkmn_flavor_en.json'):
        with open(filename, 'r') as infile:
            entries = json.load(infile)
            self.entries.extend(entries)

    def best_match(self, matchtext: str):
        if matchtext not in self._cache:
            best = min([
                (Levenshtein.distance(matchtext, x), x)
                for x in self.entries
            ])
            self._cache[matchtext] = best

        return self._cache[matchtext]


def pipeline(inpath, outpath, steps=('artist_crop', 'upscale', 'thresh')):
    image = PIL.Image.open(inpath)

    for i, step in enumerate(steps):
        phase = f"{i:02d}-{step}"
        image = OPERATIONS[step](image)
        image.save(mkoutdir(outpath, phase, inpath.name))

    return image


# All of these are f(Image) -> Image
# You'll need to convert to/from numpy arrays to use cv2 filters.
# see otsu_binarization for an example.
OPERATIONS = {
    'artist_crop': artist_crop,
    'flavor_crop': flavor_crop,

    '2x': lambda img: img.resize(
        (img.width * 2, img.height * 2),
        resample=PIL.Image.Resampling.LANCZOS
    ),
    '3x': lambda img: img.resize(
        (img.width * 3, img.height * 3),
        resample=PIL.Image.Resampling.LANCZOS
    ),
    '4x': lambda img: img.resize(
        (img.width * 4, img.height * 4),
        resample=PIL.Image.Resampling.LANCZOS
    ),
    'half': lambda img: img.resize(
        (round(img.width / 2), round(img.height /2)),
        resample=PIL.Image.Resampling.LANCZOS
    ),

    'otsu': otsu_binarization,
    'blur': gaussian_blur,
    'erode_dilate': erode_dilate,
    'gray': ImageOps.grayscale,
    'invert': ImageOps.invert,

    'nagothresh': lambda img: imgfilter(
        img,
        proximity_filter(
            color=BLACK,
            bgcolor=WHITE,
            lower=125,
            upper=175
        )
    ),

    # Might require the grayscale pass first in order to work.
    'bin50': lambda img: img.point(binarization_filter(0.50)),
    'invbin50': lambda img: img.point(binarization_filter(0.50, True)),
}


def mkparser():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--input',
        metavar='INPUT_DIR',
        help="Path to load images from. Non-recursive.",
        default='input/typical',
    )

    parser.add_argument(
        '--output',
        metavar='OUTPUT_DIR',
        help="Path to store intermediate images to. Default 'output'.",
        default='output',
    )

    parser.add_argument(
        '--interactive',
        action='store_true',
        help="Interactive mode for correcting low-confidence guesses",
    )

    parser.add_argument(
        '--flavor',
        action='store_true',
        help="Flavor text (instead of artist)",
    )

    parser.add_argument(
        '--steps',
        metavar='STEPS',
        nargs='+',
        help=(
            "Transformation steps to apply."
            " Try upscale thresh or thresh upscale."
        )
    )

    parser.add_argument(
        '--oem',
        default='1',
        help="Passed to tesseract."
    )

    parser.add_argument(
        '--psm',
        default='7',
        help="Passed onward to tesseract."
    )

    return parser


def main():
    parser = mkparser()
    args, unknown = parser.parse_known_args()

    if args.flavor:
        args.tessargs = " ".join(
            ['--oem', args.oem] + unknown
        )
    else:
        args.tessargs = " ".join(
            ['--oem', args.oem, '--psm', args.psm] + unknown
        )

    textpool = TextPool()
    if args.flavor:
        textpool.load_zukan()
        key = 'flavor'
    else:
        textpool.load_artists()
        key = 'artist'

    path = Path(args.input)
    outpath = Path(args.output)

    miss = 0
    hit = 0
    accuracies = []

    try:
        with open("results.json", "r") as infile:
            results = json.load(infile)
    except FileNotFoundError:
        results = {}

    for filename in sorted(os.listdir(path)):
        accept = False
        image = pipeline(
            path.joinpath(filename),
            outpath,
            ['flavor_crop' if args.flavor else 'artist_crop'] + args.steps
        )

        raw_text = pytesseract.image_to_string(
            image,
            config=args.tessargs,
        )
        raw_text = " ".join(raw_text.split())
        dist, text = textpool.best_match(raw_text)
        accuracy = 1 - (dist / len(text))

        if accuracy < 0.75:
            if args.interactive:
                with tempfile.NamedTemporaryFile() as f:
                    image.save(f, format="png")
                    answ = prompt_user(
                        confidence=accuracy,
                        rawtext=raw_text,
                        guesstext=text,
                        image_file=f.name
                    )
                # User just accepted what we proposed directly
                if answ == text:
                    accept = True
                else:
                    # User typed something manually, we should normalize it.
                    # This is nice to get 'pokemon' => 'Pokémon'.
                    new_dist, new_text = textpool.best_match(answ)
                    new_accuracy = 1 - (new_dist / len(new_text))

                    # User's input matched something we have pretty closely,
                    # Accept this answer.
                    if new_accuracy >= 0.95:
                        text = new_text
                        accept = True
                    else:
                        print("Entered transcription was not found in the text pool. (Skipping.)")
                        print(f"Text:  '{answ}'")
                        print(f"Match: '{new_text}'")
                        print(f"Similarity: {new_accuracy:.2f}")
            else:
                text = f"????? got '{raw_text}' maybe '{text}'?"
        else:
            # Accuracy >= 75%
            accept = True

        if accept:
            hit += 1
            results.setdefault(filename, {})[key] = text
        else:
            miss += 1

        accuracies.append(accuracy)

        name = filename.ljust(40)
        conf = f"{round(accuracy * 100, 2)}%".rjust(6)
        print(f"{name} - {conf} - '{text}'")

    print("===================================")
    print(args)
    print(f"hits: {hit}")
    print(f"misses: {miss}")
    av_acc = sum(accuracies) / len(accuracies)
    print(f"accuracy: {av_acc}")
    print("===================================")

    with open("results.json", "w") as outfile:
        json.dump(results, outfile, indent=2, ensure_ascii=False)


if __name__ == '__main__':
    main()
